import { Component } from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent  {

  //ISTANCE
  id :number;

  //CONSTRUCTOR
  constructor(private route: ActivatedRoute) 
  {
    this.id = this.route.snapshot.params['id'];    
  } 

}
