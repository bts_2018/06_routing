import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ProductsComponent } from './products/products.component';
import { RouterModule } from '@angular/router';
import { CategoriesComponent } from './categories/categories.component';
import { ProductComponent } from './product/product.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    CategoriesComponent,
    ProductComponent,
 
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
          path : "products",component: ProductsComponent
      },
      {
        path : "categories",component: CategoriesComponent
      },
      { 
        path: 'product/:id', component: ProductComponent 
      }
 
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
