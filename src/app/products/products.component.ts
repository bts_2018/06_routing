import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ActivatedRoute, Params }   from '@angular/router';
import { Product }   from '../classes/Product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent  {

  //ISTANCE
  products:Product[];

   //CONSTRUCTOR
   constructor(private route: ActivatedRoute) 
   {
    this.products = [
      { 
        "id":1 , "name" :"BMW"
      },
      { 
        "id":2 , "name" :"Moto XMAX 250"
      },
      { 
        "id":3 , "name" :"Samnsug S8"
      }
    ];

   } 
   
}
