import { Component, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ActivatedRoute, Params }   from '@angular/router';
import { Category }   from '../classes/Category';

@Component({
  selector: 'app-products',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent  {

  //ISTANCE
  categories:Category[];

   //CONSTRUCTOR
   constructor(private route: ActivatedRoute) 
   {
    this.categories = [
      { 
        "idCategory":1 , "categoryname" :"Books"
      },
      { 
        "idCategory":2 , "categoryname" :"Cars"
      },
      { 
        "idCategory":3 , "categoryname" :"Movies"
      }
    ];

   } 
   
}
